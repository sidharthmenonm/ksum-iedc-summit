import Vue from 'vue';
import VueSimpleMarkdown from 'vue-simple-markdown'
import speaker_list from './speakers';
import schedule_list from './events';

Vue.use(VueSimpleMarkdown)

new Vue({ 
  el: '#schedule-section',
  data: {
    speakers : speaker_list,
    schedule : schedule_list,
    filter_venue: "",
    filter_cat: ""
  },
  computed:{
    filtered(){
      var filtered = this.schedule;
      var vm = this;
      if(this.filter_venue){
        filtered = filtered.filter(function(item){
          return item.venue==vm.filter_venue
        })
      }
      if(this.filter_cat){
        filtered = filtered.filter(function(item){
          return item.category==vm.filter_cat
        })
      }
      return filtered;
    },
    venues(){
      var venue = this.schedule.map(function(item){
        return item.venue;
      })
      return [... new Set(venue)];
    },
    categories(){
      var venue = this.schedule.map(function(item){
        return item.category;
      })
      return [... new Set(venue)];
    }
  }
});
