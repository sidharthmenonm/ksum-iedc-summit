module.exports = [
  {
    "date": "Saturday October 19, 2019",
    "start": "9:30 AM",
    "end": "10:00 AM",
    "item": "WarmUp Music",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:00 AM",
    "end": "10:02 AM",
    "item": "Prayer",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:02 AM",
    "end": "10:07 AM",
    "item": "Welcome address",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [14],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:07 AM",
    "end": "10:12 AM",
    "item": "About IEDC Summit",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [2],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:12 AM",
    "end": "10:17 AM",
    "item": "Presidential Address",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [3],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:17 AM",
    "end": "10:20 AM",
    "item": "Lighting of the Lamp/ Inauguration",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:20 AM",
    "end": "10:40 AM",
    "item": "Inaugural Address",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [11],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:40 AM",
    "end": "10:45 AM",
    "item": "Special Address",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [5],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:45 AM",
    "end": "10:48 AM",
    "item": "Felicitation",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [15],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "10:48 AM",
    "end": "11:00 AM",
    "item": "Vote of Thanks",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [16],
    "description": "",
    "category": "Inaguration",
    "class": "ev_0"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "11:00 AM",
    "end": "11:20 AM",
    "item": "Keynote Address",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [1],
    "description": "",
    "category": "Keynote",
    "class": "ev_1"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "4:30 PM",
    "end": "5:00 PM",
    "item": "Power Talk",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [17],
    "description": "",
    "category": "Talks",
    "class": "ev_2"
  },
  {
    "date": "Saturday October 19, 2019",
    "start": "5:00 PM",
    "end": "5:30 PM",
    "item": "Valedictory function",
    "venue": "Main Stage: Multipurpose Indoor Stadium",
    "speakers": [],
    "description": "",
    "category": "Valedictory",
    "class": "ev_3"
  }
]