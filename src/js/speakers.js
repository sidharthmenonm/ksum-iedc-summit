module.exports = [
  {
    "id": 1,
    "order": 6,
    "name": "Deena Jacob",
    "designation": "Co Founder & CFO",
    "company": "Open Financial Technologies",
    "image": "deena-jacob.webp",
    "link": "https://www.linkedin.com/in/deena-jacob-348b8b4/"
  },
  {
    "id": 2,
    "order": 4,
    "name": "Dr.Saji Gopinath",
    "designation": "CEO",
    "company": "KSUM",
    "image": "saji-gopinath.webp",
    "link": "https://www.linkedin.com/in/saji-gopinath-82774046/"
  },
  {
    "id": 3,
    "order": 3,
    "name": "Sri. M Sivasankar IAS",
    "designation": "Secretary IT",
    "company": "Govt. of Kerala",
    "image": "it-secretary.webp",
    "link": "#"
  },
  {
    "id": 4,
    "order": 10,
    "name": "Anto P Biju",
    "designation": "Co-Founder",
    "company": "Lamaara Pvt Ltd",
    "image": "anto-lamaara.webp",
    "link": "https://www.linkedin.com/in/anto-patrex/"
  },
  {
    "id": 5,
    "order": 5,
    "name": "Dr. Rajasree M.S",
    "designation": "Vice Chancellor",
    "company": "A P J Abdul Kalam Technological University",
    "image": "rajasree-ms.webp",
    "link": "https://www.linkedin.com/in/rajasree-m-s/"
  },
  {
    "id": 6,
    "order": 11,
    "name": "Pradeep P S",
    "designation": "CEO",
    "company": "Farmers Fresh Zone",
    "image": "pradeed-ps.webp",
    "link": "https://www.linkedin.com/in/pradeep-p-s/"
  },
  {
    "id": 7,
    "order": 1,
    "name": "Prof. C. Raveendranath",
    "designation": "Minister for Education",
    "company": "Govt. of Kerala",
    "image": "minister.webp",
    "link": "https://en.wikipedia.org/wiki/C._Raveendranath"
  },
  {
    "id": 8,
    "order": 12,
    "name": "Savio victor",
    "designation": "Co Founder & CFO",
    "company": "Neuroplex",
    "image": "savio-nueroplex.webp",
    "link": "https://www.linkedin.com/in/saviovictor/"
  },
  {
    "id": 9,
    "order": 13,
    "name": "Nikhil NP",
    "designation": "CTO",
    "company": "Genrobotics",
    "image": "nikhil-genrobotics.jpg",
    "link": "https://www.linkedin.com/in/nikhil-np-7539a5137/"
  },
  {
    "id": 10,
    "order": 9,
    "name": "Sreekumar V",
    "designation": "Head Operations",
    "company": "iBoson Innovations",
    "image": "sreekumar.jpg",
    "link": "https://www.linkedin.com/in/sreekumar-v/"
  },
  {
    "id": 11,
    "order": 2,
    "name": "Dr. K Radhakrishnan",
    "designation": "Ex. Chairman",
    "company": "ISRO",
    "image": "isro.jpg",
    "link": "https://en.wikipedia.org/wiki/K._Radhakrishnan"
  },
  {
    "id": 12,
    "order": 7,
    "name": "Bian Li",
    "designation": "Founder",
    "company": "Puzzlemaster",
    "image": "bianli.jpg",
    "link": "https://www.linkedin.com/in/beebianli/"
  },
  {
    "id": 13,
    "order": 8,
    "name": "Jainaressh BC",
    "designation": "AM- Education",
    "company": "Unity Technologies",
    "image": "jainaresh.jpg",
    "link": "https://www.linkedin.com/in/jainaressh-bc-48179b83/"
  },
  {
    "id": 14,
    "order": 0,
    "name": "Fr. George Pareman",
    "designation": "Exe. Director",
    "company": "SCET",
    "image": "",
    "link": ""
  },
  {
    "id": 15,
    "order": 0,
    "name": "Dr. Elizebeth Elias",
    "designation": "Director",
    "company": "SCET",
    "image": "",
    "link": ""
  },
  {
    "id": 16,
    "order": 0,
    "name": "Dr. Nixon Kuruvila",
    "designation": "Principal",
    "company": "SCET",
    "image": "",
    "link": ""
  },
  {
    "id": 17,
    "order": 0,
    "name": "Mr. Santhosh Kulangara",
    "designation": "MD",
    "company": "Safari T.V",
    "image": "",
    "link": ""
  }
]