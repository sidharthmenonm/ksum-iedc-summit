import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import SmoothScroll from 'smooth-scroll';
import magnificPopup from 'magnific-popup';
//import SA from './redirection-mobile';

window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;


// $.magnificPopup.open({
//   items: {
//     src: 'reshed.webp'
//   },
//   type: 'image'
// });

// $('.img-pop-up').magnificPopup({
//   type: 'image',
//   gallery: {
//       enabled: true
//   }
// });

// SA.redirection_mobile({
//   mobile_url : "iedcsummit.in/app.html",
//   mobile_prefix : "https"
// });

var scroll = new SmoothScroll('a[data-scroll]');

$( '.hamburger-menu' ).on( 'click', function() {
  $(this).toggleClass('open');
  $('.site-navigation').toggleClass('show');
});

var deferredPrompt;

window.addEventListener('beforeinstallprompt', function (e) {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;

  showAddToHomeScreen();

});

function showAddToHomeScreen() {

  if($(window).width()<1200){
    var a2hsBtn = document.querySelector(".buttonDownload");
    a2hsBtn.style.display = "block";
    a2hsBtn.addEventListener("click", addToHomeScreen);
  }
}

function addToHomeScreen() { 
  var a2hsBtn = document.querySelector(".buttonDownload");  // hide our user interface that shows our A2HS button
  a2hsBtn.style.display = 'none';  // Show the prompt
  deferredPrompt.prompt();  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
    .then(function(choiceResult){

  if (choiceResult.outcome === 'accepted') {
    console.log('User accepted the A2HS prompt');
  } else {
    console.log('User dismissed the A2HS prompt');
  }

  deferredPrompt = null;

});}
