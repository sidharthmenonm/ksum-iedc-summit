import VueRouter from 'vue-router'
import sched_expaned from './sched.vue';
import sched_simple from './simple.vue';
import sched_venue from './venue.vue';
import sched_single from './single.vue';

const routes = [
  { name: 'simple', path: '/', component: sched_simple, props:true },
  { name: 'expanded', path: '/expanded', component: sched_expaned, props: true },
  { name: 'venue', path: '/venue', component: sched_venue, props: true }, 
  { name: 'details', path: '/:id', component: sched_single, props: true }, 

]

const router = new VueRouter({
  routes
})

module.exports = router;