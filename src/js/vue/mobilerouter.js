import VueRouter from 'vue-router'
import sched_expaned from './sched.vue';
import sched_simple from './simple.vue';
import sched_venue from './venue.vue';
import sched_single from './single.vue';
import mobile_home from './mobilehome.vue';
import sched from './app.vue';
import register from './register.vue';
import notification from './notification.vue';
import location from './location.vue';

const routes = [
  { name: 'home', path: '/', component: mobile_home },
  { name: 'schedule', path: '/schedule', component: sched,
    children: [
      { name: 'simple', path: 'simple', component: sched_simple, props:true },
      { name: 'expanded', path: 'expanded', component: sched_expaned, props: true },
      { name: 'venue', path: 'venue', component: sched_venue, props: true }, 
      { name: 'details', path: ':id', component: sched_single, props: true }, 
    ]
  },
  { name: 'register', path: '/register', component: register },
  { name: 'notification', path: '/notification', component: notification },
  { name: 'location', path: '/location', component: location }
]

const router = new VueRouter({
  routes
})

module.exports = router;