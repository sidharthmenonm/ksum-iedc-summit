import Vue from 'vue';
import VueSimpleMarkdown from 'vue-simple-markdown'
import VueRouter from 'vue-router'
import router from './vue/router'
import App from './vue/app.vue'

Vue.use(VueRouter)
Vue.use(VueSimpleMarkdown)

const app = new Vue({
  el: "#app",
  router,
  render: h => h(App)
})